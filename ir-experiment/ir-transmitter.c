/* IR Transmitter for ATmega32U4
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ir-transmitter.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

volatile const PulseCount *pulse_blocks;
volatile BlockIndex pulse_block_count = 0;

void ir_transmitter_send(const PulseCount *blocks, BlockIndex block_count)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    pulse_blocks = blocks;
    pulse_block_count = block_count;
  }
  ir_transmitter_enable();
}

void ir_transmitter_enable()
{
  TC4H = 0;
  TCNT4 = 0;
  TCCR4A |= (0b11 << COM4A0);  // Enable OC4A connection
  TCCR4B |= (0b0001 << CS40);  // Turn on timer 4
  // PORTD |= (1 << PORTD4);      // Turn on debug output
}

void ir_transmitter_disable()
{
  TCCR4A &= ~(0b11 << COM4A0);  // Disable OC4A connection
  TCCR4B &= ~(0b1111 << CS40);  // Turn off timer 4
  // PORTD &= ~(1 << PORTD4);      // Turn off debug output
}

bool ir_transmitter_is_enabled()
{
  return (TCCR4B & (0b1111 << CS40)) != 0;
}

static PulseCount next_pulse_block(void)
{
  static BlockIndex pulse_block_index = 0;

  // if (pulse_block_index == 0) {
  //   PORTD |= (1 << PORTD6);
  //   PORTD &= ~((1 << PORTD6));
  // }

  if (pulse_block_index == pulse_block_count - 1) {
    pulse_block_index = 0;
    return 0;
  }

  PulseCount next_block = pulse_blocks[(pulse_block_index++) % pulse_block_count];
  return next_block;
}

ISR(TIMER4_OVF_vect)
{
  static bool first = true;
  static PulseCount desired_pulse_count = 0;
  static PulseCount pulse_count = 0;

  if (first) {
    desired_pulse_count = next_pulse_block();
    first = false;
  }

  pulse_count++;
  if (pulse_count < desired_pulse_count) {
    return;
  }

  TCCR4A ^= (0b11 << COM4A0);  // Toggle OC4A connection
  // PORTD ^= (1 << PORTD4);      // Toggle debug output

  pulse_count = 0;
  desired_pulse_count = next_pulse_block();
  if (desired_pulse_count == 0) {
    first = true;
    ir_transmitter_disable();
    return;
  }
}

void ir_transmitter_setup()
{
  // OUTPUT on PC7
  DDRC |= (1 << DDD7);
  PORTC &= ~(1 << PORTC7);

  // Set up PWM on PC7:
  PLLFRQ = 0;
  TCCR4A = (0b11 << COM4A0) | (1 << PWM4A);
  TCCR4B = (0b0001 << CS40);
  TCCR4D = (0b00 << WGM40);
  TIMSK4 = 1 << TOIE4;

  // 16 MHz / 38 KHz = 421.053, so count to 420.
  TC4H = 420 >> 8;
  OCR4C = 420 & 0xFF;

  TC4H = 0;
  OCR4A = 210;

  ir_transmitter_disable();
}
