# IR Transmitter for ATmega32U4
# Copyright (C) 2020 dr. Sybren A. Stüvel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from pathlib import Path
from pprint import pprint

if len(sys.argv) != 2:
    raise SystemExit(f"Usage: {sys.argv[0]} somefile.csv")

csv_path = Path(sys.argv[1])
csv_file = csv_path.open('r')

# Read first 9 lines of metadata
metadata = {}
for _ in range(9):
    line = csv_file.readline().strip()
    key, value = line.split(':', 1)
    metadata[key] = value

pprint(metadata)

# Read the rest as CSV, and report crossovers
csv_file.readline()  # skip header
upper_threshold_voltage = 4.0
lower_threshold_voltage = 1.0


def parse_line(line: str):
    time, voltage = line.strip().split(',')
    return float(time), float(voltage)


last_time, last_voltage = parse_line(csv_file.readline())
if last_voltage > upper_threshold_voltage:
    threshold = 'LOWER'
elif last_voltage < lower_threshold_voltage:
    threshold = 'UPPER'
else:
    raise SystemExit('Not starting above/below thresholds')

pulses = []
is_first = True


def report_crossing(time, voltage, last_time, last_voltage, last_level: str):
    global is_first
    if is_first:
        # The first crossing is where we should start measuring. Before that it's bogus.
        is_first = False
        return

    time_diff = time - last_time
    nr_of_pulses = round(time_diff * 38000)  # 38 kHz
    print(f"on {time: .6f}: from {last_voltage:4.2f} to {voltage:4.2f} "
          f"time diff: {time_diff:.6f} = {nr_of_pulses:3d} pulses ({last_level})")

    pulses.append(nr_of_pulses)


for line in csv_file:
    time, voltage = parse_line(line)

    if threshold == 'LOWER' and voltage < lower_threshold_voltage:
        report_crossing(time, voltage, last_time, last_voltage, 'HIGH')
        threshold = 'UPPER'
        last_time, last_voltage = time, voltage
    elif threshold == 'UPPER' and voltage > upper_threshold_voltage:
        report_crossing(time, voltage, last_time, last_voltage, 'low')
        threshold = 'LOWER'
        last_time, last_voltage = time, voltage

pulse_str = str(pulses)[1:-1]
print(f"pulses[{len(pulses)}] = {{{pulse_str}}}")
