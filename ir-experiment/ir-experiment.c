/* IR Experiment for ATmega32U4
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "button-debounced.h"
#include "ir-transmitter.h"

#include <avr/io.h>
#include <avr/interrupt.h>

const PulseCount pulses_vol_up[] = {  // BOSE volume UP
    40, 54, 20, 16, 22, 16, 21, 53, 22, 54, 20, 53, 22, 54, 20, 53, 22, 54,
    20, 54, 20, 54, 20, 16, 22, 16, 21, 16, 22, 16, 21, 17, 22, 16, 21, 1900,
    40, 53, 21, 16, 21, 17, 21, 53, 21, 55, 19, 55, 20, 54, 20, 54, 19, 55,
    20, 54, 20, 53, 22, 16, 21, 17, 22, 16, 22, 16, 21, 16, 22, 16, 21};
const PulseCount pulses_vol_down[] = {  // BOSE volume DOWN
    40, 54, 21, 53, 21, 16, 21, 55, 20, 54, 21, 54, 21, 53, 22, 53, 22, 54,
    19, 17, 21, 54, 20, 16, 21, 16, 22, 16, 21, 16, 21, 17, 22, 16, 22, 1900,
    40, 53, 22, 53, 20, 17, 22, 54, 19, 54, 21, 54, 19, 54, 22, 54, 21, 53,
    21, 16, 22, 54, 21, 16, 22, 16, 21, 16, 22, 16, 21, 16, 22, 16, 21};
const PulseCount pulses_tv_power[] = {  // TV Power button
    173, 166, 25, 61, 24, 61, 24, 61,   24,  17,  24, 19, 24, 18, 25, 18, 25, 17, 25, 61,
    24,  61,  24, 61, 24, 17, 25, 18,   25,  17,  24, 19, 25, 17, 25, 18, 24, 18, 24, 18,
    25,  61,  24, 17, 25, 61, 23, 19,   25,  17,  25, 59, 24, 62, 24, 61, 24, 17, 24, 62,
    23,  18,  25, 61, 24, 59, 25, 1791, 172, 167, 24, 62, 23, 62, 23, 61, 25, 17, 25, 18,
    25,  17,  25, 18, 25, 17, 24, 62,   23,  62,  23, 62, 23, 18, 24, 19, 25, 17, 24, 19,
    24,  18,  24, 19, 25, 17, 24, 18,   25,  59,  25, 17, 25, 59, 25, 18, 25, 17, 25, 61,
    23,  62,  23, 61, 25, 17, 24, 62,   24,  17,  25, 61, 23, 62, 24};
const PulseCount pulses_tv_pmode[] = {  // TV Picture Mode
    173, 166, 24, 61, 25, 61, 23, 61,   24,  19,  24, 19, 23, 18, 25, 18, 25, 17, 24, 59,
    24,  60,  25, 60, 25, 17, 25, 18,   25,  17,  25, 18, 25, 17, 25, 18, 23, 60, 24, 20,
    23,  17,  25, 19, 23, 19, 23, 20,   23,  18,  23, 61, 25, 17, 24, 61, 23, 61, 24, 60,
    24,  61,  24, 60, 23, 62, 23, 1793, 173, 166, 25, 59, 25, 59, 25, 60, 24, 18, 24, 19,
    24,  18,  24, 19, 24, 18, 24, 61,   24,  60,  24, 61, 25, 17, 25, 18, 25, 17, 25, 18,
    25,  17,  25, 17, 24, 60, 24, 19,   25,  17,  25, 18, 25, 17, 25, 18, 25, 17, 24, 61,
    25,  17,  23, 62, 23, 60, 25, 59,   24,  61,  24, 60, 25, 59, 25};

int main()
{
  // Enable pullups on unused pins to ensure stable levels.
  DDRB = DDRC = DDRD = DDRE = DDRF = 0;
  PORTB = PORTC = PORTD = PORTE = PORTF = 255;

  ir_transmitter_setup();
  sei();

  ButtonHistory button_vol_up;
  ButtonHistory button_vol_down;
  ButtonHistory button_tv_power;
  ButtonHistory button_tv_pmode;
  for (;;) {
    button_update(&button_vol_up, (PIND & (1 << PIND6)) == 0);
    button_update(&button_vol_down, (PIND & (1 << PIND7)) == 0);
    button_update(&button_tv_power, (PINB & (1 << PINB5)) == 0);
    button_update(&button_tv_pmode, (PINB & (1 << PINB4)) == 0);

    if (!ir_transmitter_is_enabled()) {
      if (button_is_pressed(button_vol_up)) {
        IR_TRANSMIT(pulses_vol_up);
      }
      else if (button_is_pressed(button_vol_down)) {
        IR_TRANSMIT(pulses_vol_down);
      }
      else if (button_is_pressed(button_tv_power)) {
        IR_TRANSMIT(pulses_tv_power);
      }
      else if (button_is_pressed(button_tv_pmode)) {
        IR_TRANSMIT(pulses_tv_pmode);
      }
    }
  }
  return 0;
}
