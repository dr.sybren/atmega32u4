/*
  Copyright 2017  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

#include "Keyboard.h"
#include "button-debounced.h"

#include <avr/io.h>
#include <stdbool.h>

#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <string.h>

/** Buffer to hold the previously generated Keyboard HID report, for comparison purposes inside the
 * HID class driver. */
static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Keyboard_HID_Interface = {
    .Config =
        {
            .InterfaceNumber = INTERFACE_ID_Keyboard,
            .ReportINEndpoint =
                {
                    .Address = KEYBOARD_EPADDR,
                    .Size = KEYBOARD_EPSIZE,
                    .Banks = 1,
                },
            .PrevReportINBuffer = PrevKeyboardHIDReportBuffer,
            .PrevReportINBufferSize = sizeof(PrevKeyboardHIDReportBuffer),
        },
};

volatile bool button_pressed = false;
volatile bool any_button_pressed = false;

volatile struct {
  ButtonHistory start_stop;
  ButtonHistory pause;
  ButtonHistory scene1;
  ButtonHistory scene2;
  ButtonHistory scene3;
} buttons;

static void detect_buttons(void)
{
  bool pressed = false;
  pressed |= button_update(&buttons.start_stop, (PINB & (1 << PINB5)) == 0);
  pressed |= button_update(&buttons.pause, (PINB & (1 << PINB4)) == 0);
  pressed |= button_update(&buttons.scene1, (PIND & (1 << PIND7)) == 0);
  pressed |= button_update(&buttons.scene2, (PIND & (1 << PIND6)) == 0);
  pressed |= button_update(&buttons.scene3, (PIND & (1 << PIND4)) == 0);
  button_pressed = pressed;
}

ISR(TIMER4_OVF_vect)
{
  static uint16_t pwm_value = 64;

  detect_buttons();

  if (button_pressed) {
    pwm_value = 256;
  }
  else {
    if (pwm_value > 16) {
      pwm_value -= 16;
    }
    else if (any_button_pressed) {
      pwm_value = 4;
    }
    else {
      pwm_value = 0;
    }
  }

  // pwm_value = buttons.right_arrow;

  TC4H = (pwm_value >> 8) & 0b11;
  OCR4A = pwm_value & 0xFF;
}

int main(void)
{
  // Enable pullups on unused pins to ensure stable levels.
  DDRB = DDRD = DDRE = DDRF = 0;
  PORTB = PORTC = PORTD = PORTE = PORTF = 255;

  // OUTPUT: PC7 LED
  DDRC = (1 << DDD7);
  // PORTC &= ~(1 << PORTC7);
  // PORTC |= (1 << PORTC7);

  button_pressed = true;

  // Set up PWM on PC7:
  PLLFRQ = 0;
  TCCR4A = (0b10 << COM4A0) | (1 << PWM4A);
  TCCR4B = (0b1000 << CS40);
  TCCR4D = (0b00 << WGM40);
  TIMSK4 = 1 << TOIE4;

  TC4H = 0;
  OCR4C = 255;  // set TOP value of timer 4

  USB_Init();
  sei();

  for (;;) {
    // detect_buttons();
    HID_Device_USBTask(&Keyboard_HID_Interface);
    USB_USBTask();
  }
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool config_success = true;

  config_success &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);
  USB_Device_EnableSOFEvents();

  if (!config_success) {
    PORTD |= (1 << PORTD6);
  }
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
  HID_Device_MillisecondElapsed(&Keyboard_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure
 * being referenced \param[in,out] ReportID    Report ID requested by the host if non-zero,
 * otherwise callback should set to the generated report ID \param[in]     ReportType  Type of the
 * report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature \param[out]    ReportData
 * Pointer to a buffer where the created report should be stored \param[out]    ReportSize  Number
 * of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library
 * determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
                                         uint8_t *const ReportID,
                                         const uint8_t ReportType,
                                         void *ReportData,
                                         uint16_t *const ReportSize)
{
  USB_KeyboardReport_Data_t *report = (USB_KeyboardReport_Data_t *)ReportData;
  uint8_t used_key_codes = 0;

  if (button_is_pressed(buttons.start_stop)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_SHIFT;
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_PAUSE;
  }
  if (button_is_pressed(buttons.pause)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_CONTROL;
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_PAUSE;
  }
  if (button_is_pressed(buttons.scene1)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_SHIFT;
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_SCROLL_LOCK;
  }
  if (button_is_pressed(buttons.scene2)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_CONTROL;
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_SCROLL_LOCK;
  }
  if (button_is_pressed(buttons.scene3)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_ALT;
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_SCROLL_LOCK;
  }

  any_button_pressed = used_key_codes > 0;

  *ReportSize = sizeof(USB_KeyboardReport_Data_t);
  return false;
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being
 * referenced \param[in] ReportID    Report ID of the received report from the host \param[in]
 * ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or
 * HID_REPORT_ITEM_Feature \param[in] ReportData  Pointer to a buffer where the received report has
 * been stored \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void *ReportData,
                                          const uint16_t ReportSize)
{
}
