#include "button-debounced.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

ISR(TIMER0_OVF_vect)
{
  static ButtonHistory button;
  // bool button_pressed = (PIND & (1 << PIND4)) == 0;
  bool button_pressed = button_update(&button, (PIND & (1 << PIND4)) == 0);
  static uint8_t pwm_value = 16;

  if (button_pressed) {
    pwm_value = 64;
  }
  else {
    if (pwm_value > 1) {
      pwm_value -= 1;
    }
    else if (button_is_pressed(button)) {
      pwm_value = 1;
    }
    else {
      pwm_value = 0;
    }
  }

  OCR0A = ~pwm_value;
}

int main()
{
  // Enable pullups on unused pins to ensure stable levels.
  DDRB = DDRC = DDRD = DDRE = DDRF = 0;
  PORTB = PORTC = PORTD = PORTE = PORTF = 255;

  // OUTPUT: PB7 LED
  DDRB |= (1 << DDD7);

  // Set up PWM on PB7:
  TCCR0A = (0b11 << COM0A0) | (0b11 << WGM00);
  TCCR0B = (0b0 << WGM02) | (0b011 << CS00);
  TIMSK0 = 1 << TOIE0;

  sei();

  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}
