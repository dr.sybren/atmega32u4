#define F_CPU 16000000

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>

volatile bool do_left = false;
volatile uint16_t value_left = 0;
volatile uint16_t value_right = 0;

ISR(ADC_vect)
{
  // value_left = value_right = ADC;
  if (do_left) {
    value_left = ADC;

    // Switch to the right, i.e. PF1/ADC1.
    do_left = false;
    ADMUX |= 1;
  }
  else {
    value_right = ADC;

    // Switch to the left, i.e. PF0/ADC0.
    do_left = true;
    ADMUX &= ~1;
  }

  ADCSRA |= (1 << ADSC);
}

void setup()
{
  // Initialize the ADC in free-running mode
  ADMUX = (1 << REFS0);      // Use AVcc as reference voltage
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADIE       // enable interrupt
           | 0b111 << ADPS0  // set prescaler to 128
      ;
  ADCSRB = 0;  // Use the default trigger source (free-running mode)

  // Set PD0 and PB7 as output
  DDRB = (1 << PB7);
  DDRD = (1 << PD0);

  TCCR0A = 1                   //
           | (0b10 << COM0A0)  // Clear OC0A on compare match (non-inverting mode)
           | (0b10 << COM0B0)  // Clear OC0B on compare match (non-inverting mode)
           | (0b11 << WGM00);  //  Fast PWM mode

  TCCR0B = 1                 //
           | (0 << WGM02)    // Fast PWM mode
           | 0b010 << CS00;  // prescaler

  // Start ADC conversion to kick things off.
  ADCSRA |= (1 << ADSC);
}

void loop()
{
  // Convert 10 bit of the ADC to the 8 bit of the PWM.
  OCR0A = value_left >> 2;
  OCR0B = value_right >> 2;

  _delay_ms(1);
}

int main(void)
{
  setup();
  sei();
  while (true)
    loop();
}
