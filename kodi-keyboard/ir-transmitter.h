/* IR Transmitter for ATmega32U4
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/****
 * Uses Timer/Counter 4 for modulating the IR emitter LED on pin PC7.
 */

#if F_CPU != 16000000
#  error 16 MHz clock required for this IR transmitter.
#endif

#include <stdbool.h>
#include <stdint.h>

typedef uint16_t PulseCount;
typedef uint8_t BlockIndex;

void ir_transmitter_setup(void);
void ir_transmitter_send(const PulseCount *blocks, BlockIndex block_count);

void ir_transmitter_enable(void);
void ir_transmitter_disable(void);
bool ir_transmitter_is_enabled(void);

#define IR_TRANSMIT(blocks) ir_transmitter_send((blocks), sizeof((blocks)) / sizeof((blocks)[0]))
