/*
  Copyright 2017  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

#include "Keyboard.h"
#include "button-debounced.h"
#include "ir-transmitter.h"

#include <avr/io.h>
#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <stdbool.h>
#include <string.h>

/** Buffer to hold the previously generated Keyboard HID report, for comparison purposes inside the
 * HID class driver. */
static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Keyboard_HID_Interface = {
    .Config =
        {
            .InterfaceNumber = INTERFACE_ID_Keyboard,
            .ReportINEndpoint =
                {
                    .Address = KEYBOARD_EPADDR,
                    .Size = KEYBOARD_EPSIZE,
                    .Banks = 1,
                },
            .PrevReportINBuffer = PrevKeyboardHIDReportBuffer,
            .PrevReportINBufferSize = sizeof(PrevKeyboardHIDReportBuffer),
        },
};

const PulseCount pulses_tv_power[] = {  // TV Power button
    173, 166, 24, 61, 25, 61, 23, 61,   24,  19,  24, 19, 23, 18, 25, 18, 25, 17, 24, 59,
    24,  60,  25, 60, 25, 17, 25, 18,   25,  17,  25, 18, 25, 17, 25, 18, 23, 60, 24, 20,
    23,  17,  25, 19, 23, 19, 23, 20,   23,  18,  23, 61, 25, 17, 24, 61, 23, 61, 24, 60,
    24,  61,  24, 60, 23, 62, 23, 1793, 173, 166, 25, 59, 25, 59, 25, 60, 24, 18, 24, 19,
    24,  18,  24, 19, 24, 18, 24, 61,   24,  60,  24, 61, 25, 17, 25, 18, 25, 17, 25, 18,
    25,  17,  25, 17, 24, 60, 24, 19,   25,  17,  25, 18, 25, 17, 25, 18, 25, 17, 24, 61,
    25,  17,  23, 62, 23, 60, 25, 59,   24,  61,  24, 60, 25, 59, 25};
const PulseCount pulses_tv_pmode[] = {  // TV Picture Mode
    173, 166, 25, 61, 24, 61, 24, 61,   24,  17,  24, 19, 24, 18, 25, 18, 25, 17, 25, 61,
    24,  61,  24, 61, 24, 17, 25, 18,   25,  17,  24, 19, 25, 17, 25, 18, 24, 18, 24, 18,
    25,  61,  24, 17, 25, 61, 23, 19,   25,  17,  25, 59, 24, 62, 24, 61, 24, 17, 24, 62,
    23,  18,  25, 61, 24, 59, 25, 1791, 172, 167, 24, 62, 23, 62, 23, 61, 25, 17, 25, 18,
    25,  17,  25, 18, 25, 17, 24, 62,   23,  62,  23, 62, 23, 18, 24, 19, 25, 17, 24, 19,
    24,  18,  24, 19, 25, 17, 24, 18,   25,  59,  25, 17, 25, 59, 25, 18, 25, 17, 25, 61,
    23,  62,  23, 61, 25, 17, 24, 62,   24,  17,  25, 61, 23, 62, 24};

volatile bool button_pressed = false;
volatile bool any_button_pressed = false;

volatile struct {
  ButtonHistory backspace;
  ButtonHistory right_arrow;
  ButtonHistory left_arrow;
  ButtonHistory down_arrow;
  ButtonHistory up_arrow;
  ButtonHistory stop;
  ButtonHistory sleep;
  ButtonHistory enter;

  ButtonHistory tv_power;
  ButtonHistory tv_pmode;
} buttons;

static void detect_buttons(void)
{
  bool pressed = false;
  pressed |= button_update(&buttons.backspace, (PINC & (1 << PINC6)) == 0);
  pressed |= button_update(&buttons.right_arrow, (PINB & (1 << PINB6)) == 0);
  pressed |= button_update(&buttons.down_arrow, (PINB & (1 << PINB5)) == 0);
  pressed |= button_update(&buttons.enter, (PINB & (1 << PINB4)) == 0);
  pressed |= button_update(&buttons.up_arrow, (PIND & (1 << PIND7)) == 0);
  pressed |= button_update(&buttons.left_arrow, (PIND & (1 << PIND6)) == 0);
  pressed |= button_update(&buttons.stop, (PIND & (1 << PIND5)) == 0);
  pressed |= button_update(&buttons.sleep, (PIND & (1 << PIND4)) == 0);

  pressed |= button_update(&buttons.tv_power, (PINF & (1 << PINF4)) == 0);
  pressed |= button_update(&buttons.tv_pmode, (PINE & (1 << PINE2)) == 0);

  button_pressed = pressed;
}

ISR(TIMER0_OVF_vect)
{
  // bool button = (PIND & (1 << PIND4)) == 0;
  static uint8_t pwm_value = 16;

  detect_buttons();

  if (button_pressed) {
    pwm_value = 64;
  }
  else {
    if (pwm_value > 2) {
      pwm_value -= 2;
    }
    else if (any_button_pressed) {
      pwm_value = 1;
    }
    else {
      pwm_value = 0;
    }
  }

  OCR0B = ~pwm_value;
}

int main(void)
{
  // Enable pullups on unused pins to ensure stable levels.
  DDRB = DDRC = DDRD = DDRE = DDRF = 0;
  PORTB = PORTC = PORTD = PORTE = PORTF = 255;

  // Set up PWM on PD0 / OC0B:
  DDRD |= (1 << DDD0);
  TCCR0A = (0b11 << COM0B0) | (0b11 << WGM00);
  TCCR0B = (0b0 << WGM02) | (0b100 << CS00);
  TIMSK0 = 1 << TOIE0;
  OCR0A = 0;
  OCR0B = 255;

  button_pressed = true;
  ir_transmitter_setup();

  USB_Init();
  sei();

  for (;;) {
    HID_Device_USBTask(&Keyboard_HID_Interface);
    USB_USBTask();

    if (!ir_transmitter_is_enabled()) {
      if (button_is_pressed(buttons.tv_power)) {
        IR_TRANSMIT(pulses_tv_power);
      }
      else if (button_is_pressed(buttons.tv_pmode)) {
        IR_TRANSMIT(pulses_tv_pmode);
      }
    }
  }
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool config_success = true;

  config_success &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);
  USB_Device_EnableSOFEvents();

  if (!config_success) {
    PORTD |= (1 << PORTD6);
  }
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
  HID_Device_MillisecondElapsed(&Keyboard_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure
 * being referenced \param[in,out] ReportID    Report ID requested by the host if non-zero,
 * otherwise callback should set to the generated report ID \param[in]     ReportType  Type of the
 * report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature \param[out]    ReportData
 * Pointer to a buffer where the created report should be stored \param[out]    ReportSize  Number
 * of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library
 * determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
                                         uint8_t *const ReportID,
                                         const uint8_t ReportType,
                                         void *ReportData,
                                         uint16_t *const ReportSize)
{
  USB_KeyboardReport_Data_t *report = (USB_KeyboardReport_Data_t *)ReportData;
  uint8_t used_key_codes = 0;

  if (button_is_pressed(buttons.backspace)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_BACKSPACE;
  }
  if (button_is_pressed(buttons.right_arrow)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_RIGHT_ARROW;
  }
  if (button_is_pressed(buttons.down_arrow)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_DOWN_ARROW;
  }
  if (button_is_pressed(buttons.enter)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_ENTER;
  }
  if (button_is_pressed(buttons.up_arrow)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_UP_ARROW;
  }
  if (button_is_pressed(buttons.left_arrow)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_LEFT_ARROW;
  }
  if (button_is_pressed(buttons.stop)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_MEDIA_STOP;
  }
  if (button_is_pressed(buttons.sleep)) {
    report->KeyCode[used_key_codes++] = HID_KEYBOARD_SC_F12;
  }

  any_button_pressed = used_key_codes > 0;

  *ReportSize = sizeof(USB_KeyboardReport_Data_t);
  return false;
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being
 * referenced \param[in] ReportID    Report ID of the received report from the host \param[in]
 * ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or
 * HID_REPORT_ITEM_Feature \param[in] ReportData  Pointer to a buffer where the received report has
 * been stored \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void *ReportData,
                                          const uint16_t ReportSize)
{
}
