/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren Stüvel.
 */

#include "button-debounced.h"

#if defined(BUTTON_DEBOUNCE_8)
const ButtonHistory BUTTON_MASK = 0b11000111;
const ButtonHistory BUTTON_PRESS = 0b00000111;
const ButtonHistory BUTTON_PRESSED = 0b11111111;
#elif defined(BUTTON_DEBOUNCE_16)
const ButtonHistory BUTTON_MASK = 0b1110000000011111;
const ButtonHistory BUTTON_PRESS = 0b0000000000011111;
const ButtonHistory BUTTON_PRESSED = 0b1111111111111111;
#else
#  error Configure either 8 or 16 bit button debounce.
#endif

bool button_update(volatile ButtonHistory *button_history, bool currently_down)
{
  *button_history <<= 1;
  *button_history |= currently_down;

  if ((*button_history & BUTTON_MASK) == BUTTON_PRESS) {
    *button_history = BUTTON_PRESSED;
    return true;
  }

  return false;
}

bool button_is_pressed(ButtonHistory button_history)
{
  return button_history == BUTTON_PRESSED;
}
