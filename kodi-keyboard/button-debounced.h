/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren Stüvel.
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#define BUTTON_DEBOUNCE_16

#if defined(BUTTON_DEBOUNCE_8)
typedef uint8_t ButtonHistory;
#elif defined(BUTTON_DEBOUNCE_16)
typedef uint16_t ButtonHistory;
#else
#  error Configure either 8 or 16 bit button debounce.
#endif

bool button_update(volatile ButtonHistory *button_history, bool currently_down);
bool button_is_pressed(ButtonHistory button_history);
